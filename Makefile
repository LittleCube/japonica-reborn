####################################################
#                                                  #
#                 everything sucks                 #
#                                                  #
####################################################

CC				:= gcc

INCLUDES		:= include include/unix

LIBS			:= -pthread
LIBDIRS			:= .

SOURCES			:= src src/unix
BUILD			:= build

export INCLUDE		:= $(foreach dir, $(INCLUDES), -I../$(dir))

SRCS			:= $(foreach f, $(SOURCES), $(notdir $(wildcard $(f)/*.c)))

export OBJS		:= $(foreach f, $(SRCS), $(notdir $(f:.c=.o)))

LDFLAGS			:= -L$(LIBDIRS) $(LIBS) `sdl2-config --libs`

TARGET			:= japo



VPATH			 = $(foreach dir, $(SOURCES), ../$(dir)) $(foreach dir, $(INCLUDES), $(dir))

OPTFLAGS		:= -Ofast -ffast-math \
				   -ffunction-sections \
				   -fmerge-all-constants -Wl,--gc-sections \
				   -flto -fomit-frame-pointer \

DEBUGOPTFLAGS	:= -g

CFLAGS			:= $(INCLUDE) $(DEBUGOPTFLAGS) -std=c17 -Wno-incompatible-pointer-types -Wno-format-truncation -Wall



.PHONY: all dev clean



all: unix

unix: | $(BUILD)
	@$(MAKE) --no-print-directory -C $(BUILD) -f ../Makefile $(OBJS) -j
	
	@echo "linking \`$(TARGET)'..."
	
	@cd build && $(CXX) $(OBJS) -o ../$(TARGET) $(LDFLAGS)

switch:
	@$(MAKE) -f switch.mk -j

link:
	nxlink -a 172.16.0.30 japonica-reborn.nro

$(BUILD):
	@mkdir $@

clean:
	@echo clean ...
	
	make clean -f switch.mk || continue
	
	@rm -f $(TARGET)
	
	@rm -rf $(BUILD)

run:
	@./$(TARGET)
