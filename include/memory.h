#pragma once

#include <common.h>

#define IF 0xFF0F
#define IE 0xFFFF

#define DIV 0xFF04
#define TIMA 0xFF05
#define TMA 0xFF06
#define TAC 0xFF07

#define LCDC 0xFF40
#define STAT 0xFF41
#define SCY 0xFF42
#define SCX 0xFF43
#define LY 0xFF44
#define LYC 0xFF45
#define BGP 0xFF47
#define OBP0 0xFF48
#define OBP1 0xFF49
#define WY 0xFF4A
#define WX 0xFF4B

extern u8 mem[0x10000];

void init_mem();

u8 mem_read(int addr);
void mem_write(u16 addr, u8 value);