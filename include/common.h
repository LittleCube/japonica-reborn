#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef unsigned char u8;
typedef unsigned short u16;

typedef signed char e8;
typedef signed short e16;

void loadROM(char *path);
void loadBIOS();
//void delay(int milliseconds);

extern u8 bios[0x100];
extern bool use_bios;