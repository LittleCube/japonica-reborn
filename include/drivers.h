#pragma once

#include <common.h>

#define W 160
#define H 144

void init_drivers();
void init_input();
void init_video(int scale);
void set_pixel(int x, int y, u8 red, u8 green, u8 blue);
void update();
void msleep(int msecs);