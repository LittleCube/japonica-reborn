#pragma once

#include <common.h>

extern u8 r[8];

#define b r[1]
#define c r[0]
#define d r[3]
#define e r[2]
#define h r[5]
#define l r[4]
#define a r[7]
#define f r[6]

extern bool z;
extern bool n;
extern bool hc;
extern bool ca;

#define op16 ((mem_read(pc + 2) << 8) | mem_read(pc + 1))

// these defines suck but they're fast

#define inc_r(x) hc = (x & 0xF) + 1 > 0xF ? 1 : 0; x += 1; z = x == 0 ? 1 : 0; n = 0;

#define dec_r(x) hc = (x & 0xF) < 1 ? 1 : 0; x -= 1; z = x == 0 ? 1 : 0; n = 1;

#define rlc_r(x) ca = (x >> 7) & 1; x <<= 1; x |= ca; z = (x == 0) ? 1 : 0; n = 0; hc = 0;
#define rrc_r(x) ca = x & 1; x >>= 1; x |= ca << 7; z = (x == 0) ? 1 : 0; n = 0; hc = 0;
#define rl_r(x) int prev7 = (x >> 7) & 1; x <<= 1; x |= ca; ca = prev7; z = (x == 0) ? 1 : 0; n = 0; hc = 0;
#define rr_r(x) int prev0 = x & 1; x >>= 1; x |= ca << 7; ca = prev0; z = (x == 0) ? 1 : 0; n = 0; hc = 0;
#define sla_r(x) ca = (x >> 7) & 1; x <<= 1; z = (x == 0) ? 1 : 0; n = 0; hc = 0;
#define sra_r(x) int prev7 = (x >> 7) & 1; ca = x & 1; x >>= 1; x |= prev7 << 7; z = (x == 0) ? 1 : 0; n = 0; hc = 0;
#define swap_r(x) int prevLow = x & 0xF; x >>= 4; x |= prevLow << 4; z = (x == 0) ? 1 : 0; n = 0; hc = 0; ca = 0;
#define srl_r(x) ca = x & 1; x >>= 1; z = (x == 0) ? 1 : 0; n = 0; hc = 0;

#define bit0(x) z = (x & 1) == 0 ? 1 : 0; n = 0; hc = 1;
#define bit1(x) z = ((x >> 1) & 1) == 0 ? 1 : 0; n = 0; hc = 1;
#define bit2(x) z = ((x >> 2) & 1) == 0 ? 1 : 0; n = 0; hc = 1;
#define bit3(x) z = ((x >> 3) & 1) == 0 ? 1 : 0; n = 0; hc = 1;
#define bit4(x) z = ((x >> 4) & 1) == 0 ? 1 : 0; n = 0; hc = 1;
#define bit5(x) z = ((x >> 5) & 1) == 0 ? 1 : 0; n = 0; hc = 1;
#define bit6(x) z = ((x >> 6) & 1) == 0 ? 1 : 0; n = 0; hc = 1;
#define bit7(x) z = ((x >> 7) & 1) == 0 ? 1 : 0; n = 0; hc = 1;

#define res0(x) x &= 0xFE;
#define res1(x) x &= 0xFD;
#define res2(x) x &= 0xFB;
#define res3(x) x &= 0xF7;
#define res4(x) x &= 0xEF;
#define res5(x) x &= 0xDF;
#define res6(x) x &= 0xBF;
#define res7(x) x &= 0x7F;

#define set0(x) x |= 0x01;
#define set1(x) x |= 0x02;
#define set2(x) x |= 0x04;
#define set3(x) x |= 0x08;
#define set4(x) x |= 0x10;
#define set5(x) x |= 0x20;
#define set6(x) x |= 0x40;
#define set7(x) x |= 0x80;

extern u16 *af;
extern u16 *bc;
extern u16 *de;
extern u16 *hl;

extern u16 pc;
extern u16 sp;

extern bool ime;
extern bool ei_set;
extern bool halted;

extern int t;

void init_cpu();
void cycle();

void updateF();
void updateFlags();

void add_hl(u16 operand);
void add_sp(e8 operand);
void ld_hl_spd(e8 operand);
void call(u16 addr);
void ret();
void rst(int vec);
void intr(int vec);
void push(u16 value);
u16 pop();

void add_a(u8 operand);
void adc_a(u8 operand);
void sub_a(u8 operand);
void sbc_a(u8 operand);
void and_a(u8 operand);
void xor_a(u8 operand);
void or_a(u8 operand);
void cp_a(u8 operand);

void cb();