#pragma once

#include <common.h>

extern u16 intern;

void timer_tick(int ticks);
u16 div_select(int select_bits);