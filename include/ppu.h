#pragma once

#include <common.h>

#define SPRDATA 0x8000

#define FRAMELEN 16

extern int line_ticks;

extern u8 ly;
extern u8 intern_wy;

extern bool vblank;

extern struct timeval this_time;
extern struct timeval last_time;

void init_ppu();
void ppu_tick(int ticks);
void scanline();
void set_spr_color(int x, int y, int color, bool palette);
int spr_color(int color_select, bool palette);
void set_bg_color(int x, int y, int color);
int bg_color(int color_select);
void vbl();
void moderate_fps();