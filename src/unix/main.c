#include <common.h>

#include <cpu.h>
#include <memory.h>
#include <timer.h>
#include <ppu.h>
#include <drivers.h>

u8 bios[0x100];
bool use_bios;

int main(int argc, char **argv)
{
	if (argc > 1)
	{
		init_mem();
		loadROM(argv[1]);
		
		if (argc > 2)
		{
			int scale = atoi(argv[2]);
			
			if (scale > 0)
			{
				init_drivers(scale);
			}
			
			else
			{
				printf("E: incorrect scale argument given, using 4\n");
				printf("usage: japo [rom path] [display scale] [non-empty: skip bios]\n");
				init_drivers(4);
			}
			
			if (argc <= 3)
			{
				loadBIOS();
			}
		}
		
		else
		{
			init_drivers(4);
		}
	}
	
	else if (argc <= 1)
	{
		printf("E: please input a ROM\nusage: japo [rom path] [display scale] [non-empty: skip bios]\n");
		return 1;
	}
	
	if (!use_bios)
	{
		printf("N: BIOS not used\n");
	}
	
	init_cpu();
	
	mem[0xFF00] = 0xFF;
	
	while (1)
	{
		cycle();
		
		timer_tick(t);
		
		ppu_tick(t);
	}
}



inline void loadBIOS()
{
	use_bios = false;
	
	FILE *fileptr;
	char *ROM;
	long filelen;
	
	char path[8] = "bios.gb\0";
	
	fileptr = fopen(path, "rb");
	
	if (fileptr == NULL)
	{
		printf("E: BIOS at path `%s\' not found, skipping...\n", path);
		return;
	}
	
	fseek(fileptr, 0, SEEK_END);
	filelen = ftell(fileptr);
	rewind(fileptr);
	
	if (filelen != 0x100)
	{
		printf("E: BIOS not 0x100 bytes in length, skipping...\n");
		return;
	}
	
	ROM = (char *) malloc(filelen * sizeof(char));
	
	int err = fread(ROM, filelen, 1, fileptr);
	
	if (err <= 0)
	{
		printf("W: unable to completely read BIOS file `%s\'\n", path);
	}
	
	fclose(fileptr);
	
	for (int i = 0; i < filelen; i++)
	{
		bios[i] = ROM[i];
	}
	
	printf("N: BIOS found!\n");
	
	use_bios = true;
}

inline void loadROM(char *path)
{
	FILE *fileptr;
	char *ROM;
	long filelen;
	
	fileptr = fopen(path, "rb");
	
	if (fileptr == NULL)
	{
		printf("E: file `%s\' not found\n", path);
		exit(1);
	}
	
	fseek(fileptr, 0, SEEK_END);
	filelen = ftell(fileptr);
	rewind(fileptr);
	
	ROM = (char *) malloc(filelen * sizeof(char));
	
	int err = fread(ROM, filelen, 1, fileptr);
	
	if (err <= 0)
	{
		printf("W: unable to completely read file `%s\'\n", path);
	}
	
	fclose(fileptr);
	
	for (int i = 0; i < filelen; i++)
	{
		mem[i] = ROM[i];
	}
}