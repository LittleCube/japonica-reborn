#include <drivers.h>

#include <memory.h>

#include <SDL2/SDL.h>

SDL_Window *window;
SDL_Renderer *renderer;

SDL_Event e;

inline void init_drivers(int given_scale)
{
	SDL_Init(SDL_INIT_VIDEO);
	
	init_video(given_scale);
}

inline void init_video(int given_scale)
{
	window = SDL_CreateWindow("JackaBoy", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, W * given_scale, H * given_scale, 0);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	
	SDL_RenderSetLogicalSize(renderer, W, H);
	
	if (window == NULL)
	{
		printf("Could not create window: %s\n", SDL_GetError());
		exit(1);
	}
	
	char title[16] = "";
	
	bool endTitle = false;
	
	for (int i = 0; (i < 16) && !endTitle; ++i)
	{
		char c[] = { (char) mem[0x134 + i] };
		
		if (c[0] != 0x00)
		{
			char tmp[16];
			strcpy(tmp, title);
			snprintf(title, 16, "%s%c", tmp, c[0]);
		}
		
		else
		{
			endTitle = true;
		}
	}
	
	if (title[0] != '\0')
	{
		SDL_SetWindowTitle(window, title);
	}
}

inline void set_pixel(int x, int y, u8 red, u8 green, u8 blue)
{
	SDL_SetRenderDrawColor(renderer, red, green, blue, 0xFF);
	
	SDL_RenderDrawPoint(renderer, x, y);
}

inline void update()
{
	SDL_RenderPresent(renderer);
	
	while (SDL_PollEvent(&e))
	{
		switch (e.type)
		{
			case SDL_WINDOWEVENT:
			{
				if (e.window.event == SDL_WINDOWEVENT_CLOSE)
				{
					exit(0);
				}
				
				break;
			}
			
			case SDL_QUIT:
			{
				exit(0);
				
				break;
			}
		}
	}
}

inline void msleep(int msec)
{
	SDL_Delay(msec);
}