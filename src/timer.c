#include <timer.h>

#include <memory.h>

u16 intern;
u16 mod_ticks;

inline void timer_tick(int ticks)
{
	intern += ticks;
	
	if ((mem[TAC] & 0b100) != 0)
	{
		mod_ticks += ticks;
		int div = div_select(mem[TAC] & 0x3);
		int tima_increase = 0;
		
		if (mod_ticks > div)
		{
			tima_increase = mod_ticks / div;
			mod_ticks -= tima_increase * div;
		}
		
		if ((mem[TIMA] + tima_increase) > 0xFF)
		{
			mem[IF] |= 0b100;
			
			mem[TIMA] = mem[TMA];
		}
		
		else
		{
			mem[TIMA] += tima_increase;
		}
	}
}

inline u16 div_select(int select_bits)
{
	switch (select_bits)
	{
		case 0:		// bit 9
		{
			return 1024;
		}
		
		case 1:		// bit 3
		{
			return 16;
		}
		
		case 2:		// bit 5
		{
			return 64;
		}
		
		case 3:		// bit 7
		{
			return 256;
		}
	}
	
	printf("E: non two bit input %d given to timer's select method\n", select_bits);
	exit(1);
}