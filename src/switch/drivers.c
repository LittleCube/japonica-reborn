#include <unistd.h>
#include <switch.h>

#include <drivers.h>

#include <memory.h>

// 1080p framebuffer
#define FB_WIDTH  1920
#define FB_HEIGHT 1080

PadState pad;

Framebuffer fb;
u32 stride;
u32* framebuf;

u32 pos;

int scale = 7;

inline void init_drivers(int given_scale)
{
	init_input();
	
	init_video(given_scale);
}

inline void init_input()
{
	// Configure our supported input layout: a single player with standard controller styles
	padConfigureInput(1, HidNpadStyleSet_NpadStandard);
	
	// Initialize the default gamepad (which reads handheld mode inputs as well as the first connected controller)
	padInitializeDefault(&pad);
}

inline void init_video(int given_scale)
{
	scale = given_scale;
	
	// Retrieve the default window
	NWindow* win = nwindowGetDefault();
	
	// Create a linear double-buffered framebuffer
	framebufferCreate(&fb, win, 256, H, PIXEL_FORMAT_RGBA_8888, 2);
	framebufferMakeLinear(&fb);
	
	// Retrieve the framebuffer (for the first time)
	framebuf = (u32*) framebufferBegin(&fb, &stride);
}

inline void set_pixel(int x, int y, u8 red, u8 green, u8 blue)
{
	pos = y*stride/sizeof(u32) + x + 48;
	framebuf[pos] = (blue << 16) | (green << 8) | red;
}

inline void update()
{
	// We're done rendering, so we end the frame here.
	framebufferEnd(&fb);
	
	// Scan the gamepad. This should be done once for each frame
	padUpdate(&pad);
	
	// padGetButtonsDown returns the set of buttons that have been newly pressed in this frame compared to the previous one
	u64 kDown = padGetButtonsDown(&pad);
	
	if (kDown & HidNpadButton_Plus)
	{
		// Close the framebuffer before we exit
		framebufferClose(&fb);
		
		// Exit program
		exit(0);
	}
	
	// Retrieve the framebuffer
	framebuf = (u32*) framebufferBegin(&fb, &stride);
}

inline void msleep(int msec)
{
	//usleep(msec * 1000);
}