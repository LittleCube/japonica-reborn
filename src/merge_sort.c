#include <stdio.h>
#include <stdlib.h>

#include <merge_sort.h>

#define CURR_A_I a_i + merges
#define CURR_B_I b_i + merges + curr_arr_len

void merge_sort(char* array)
{
	for (int curr_arr_len = 1; curr_arr_len < ARR_LEN; curr_arr_len <<= 1)
	{
		printf("curr_arr_len: %d\n", curr_arr_len);
		
		for (int merges = 0; merges < ARR_LEN; merges += curr_arr_len << 1)
		{
			printf("merges: %d\n", merges);
			
			int a_i = 0;
			int b_i = 0;
			
			static char a[ARR_LEN/2 + 1];
			int a_copy_i = 0;
			
			for (int i = merges; i < merges + (curr_arr_len << 1); ++i)
			{
				printf("caching %d\n", array[i]);
				
				a[a_copy_i] = array[i];
				
				printf("i: %d, a_i: %d, b_i: %d, limit: %d\n", i, a_i, b_i, merges + (curr_arr_len << 1));
				printf("CURR_B_I: %d\n", CURR_B_I);
				printf("array[a_i]: %d, array[b_i]: %d\n", a[a_i], array[CURR_B_I]);
				
				if (CURR_B_I >= ARR_LEN)
				{
					goto copy_a;
				}
				
				a_copy_i += 1;
				
				if (a[a_i] > array[CURR_B_I])
				{
					array[i] = array[CURR_B_I];
					b_i += 1;
					
					if (b_i >= curr_arr_len)
					{
						copy_a:
						printf("a_copy_i: %d\n", a_copy_i);
						
						while (a_i < curr_arr_len && i < ARR_LEN - 1)
						{
							printf("a_i: %d, i: %d, ARR_LEN - 1: %d\n", a_i, i, ARR_LEN - 1);
							
							i += 1;
							
							a[a_copy_i] = array[i];
							a_copy_i += 1;
							
							array[i] = a[a_i];
							a_i += 1;
						}
						
						break;
					}
				}
				
				else
				{
					array[i] = a[a_i];
					a_i += 1;
					
					if (a_i >= curr_arr_len)
					{
						while (b_i < curr_arr_len && CURR_B_I < ARR_LEN)
						{
							i += 1;
							array[i] = array[CURR_B_I];
							b_i += 1;
							printf("inner i: %d, b_i: %d, val: %d\n", i, b_i, array[i]);
						}
						
						break;
					}
				}
				
				printf("chose: %d\n", array[i]);
			}
			
			for (int i = 0; i < ARR_LEN; ++i)
			{
				printf("%d\n", array[i]);
			}
			
			printf("smash?\n");
		}
		
		printf("maybe smash?\n");
	}
	
	printf("small smash?\n");
}