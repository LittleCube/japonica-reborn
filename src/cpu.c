#include <cpu.h>
#include <memory.h>
#include <ppu.h>

#include <string.h>

u8 r[8];

bool z;
bool n;
bool hc;
bool ca;

u16 *af = &f;
u16 *bc = &c;
u16 *de = &e;
u16 *hl = &l;

#define af *af
#define bc *bc
#define de *de
#define hl *hl

// magic little-endian u16 pointer magic
u16 *temp;

u16 pc;
u16 sp;

bool ime;
bool ei_set;
bool halted;

int t;

inline void init_cpu()
{
	if (!use_bios)
	{
		a = 0x01;
		b = 0x00;
		c = 0x13;
		d = 0x00;
		e = 0xD8;
		h = 0x01;
		l = 0x4D;
		
		z = 1;
		n = 0;
		
		pc = 0x0100;
		sp = 0xFFFE;
	}
	
	else
	{
		a = 0x00;
		b = 0x00;
		c = 0x00;
		d = 0x00;
		e = 0x00;
		h = 0x00;
		l = 0x00;
		
		z = 0;
		n = 0;
		hc = 0;
		ca = 0;
		
		pc = 0x0000;
		sp = 0x0000;
	}
	
	ime = false;
	ei_set = false;
	halted = false;
	
	t = 0;
}

inline void cycle()
{
	u8 instr = mem_read(pc);
	
	if (ei_set)
	{
		ime = true;
		ei_set = false;
	}
	
	if (!halted)
	{
		switch (instr)
		{
			case 0x00:		// NOP
			{
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x08:		// LD (u16), SP
			{
				mem_write(op16, sp & 0xFF);
				mem_write(op16 + 1, sp >> 8);
				
				pc += 3;
				t = 20;
				break;
			}
			
			case 0x10:		// STOP
			{
				// TODO: stop
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x18:		// JR e8
			{
				pc += ((e8) (mem_read(pc + 1))) + 2;
				
				t = 12;
				break;
			}
			
			case 0x20:		// JR NZ, e8
			{
				if (z == 0)
				{
					pc += ((e8) (mem_read(pc + 1))) + 2;
					t = 12;
				}
				
				else
				{
					pc += 2;
					t = 8;
				}
				
				break;
			}
			
			case 0x28:		// JR Z, e8
			{
				if (z == 1)
				{
					pc += ((e8) (mem_read(pc + 1))) + 2;
					t = 12;
				}
				
				else
				{
					pc += 2;
					t = 8;
				}
				
				break;
			}
			
			case 0x30:		// JR NC, e8
			{
				if (ca == 0)
				{
					pc += ((e8) (mem_read(pc + 1))) + 2;
					t = 12;
				}
				
				else
				{
					pc += 2;
					t = 8;
				}
				
				break;
			}
			
			case 0x38:		// JR C, e8
			{
				if (ca == 1)
				{
					pc += ((e8) (mem_read(pc + 1))) + 2;
					t = 12;
				}
				
				else
				{
					pc += 2;
					t = 8;
				}
				
				break;
			}
			
			case 0x01:		// LD BC, u16
			{
				bc = op16;
				
				pc += 3;
				t = 12;
				break;
			}
			
			case 0x09:		// ADD HL, BC
			{
				add_hl(bc);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x11:		// LD DE, u16
			{
				de = op16;
				
				pc += 3;
				t = 12;
				break;
			}
			
			case 0x19:		// ADD HL, DE
			{
				add_hl(de);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x21:		// LD HL, u16
			{
				hl = op16;
				
				pc += 3;
				t = 12;
				break;
			}
			
			case 0x29:		// ADD HL, HL
			{
				add_hl(hl);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x31:		// LD SP, u16
			{
				sp = op16;
				
				pc += 3;
				t = 12;
				break;
			}
			
			case 0x39:		// ADD HL, SP
			{
				add_hl(sp);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x02:		// LD (BC), A
			{
				mem_write(bc, a);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x0A:		// LD A, (BC)
			{
				a = mem_read(bc);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x12:		// LD (DE), A
			{
				mem_write(de, a);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x1A:		// LD A, (DE)
			{
				a = mem_read(de);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x22:		// LD (HL++), A
			{
				mem_write(hl, a);
				
				hl += 1;
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x2A:		// LD A, (HL++)
			{
				a = mem_read(hl);
				
				hl += 1;
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x32:		// LD (HL--), A
			{
				mem_write(hl, a);
				
				hl -= 1;
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x3A:		// LD A, (HL--)
			{
				a = mem_read(hl);
				
				hl -= 1;
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x03:		// INC BC
			{
				bc += 1;
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x0B:		// DEC BC
			{
				bc -= 1;
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x13:		// INC DE
			{
				de += 1;
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x1B:		// DEC DE
			{
				de -= 1;
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x23:		// INC HL
			{
				hl += 1;
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x2B:		// DEC HL
			{
				hl -= 1;
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x33:		// INC SP
			{
				sp += 1;
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x3B:		// DEC SP
			{
				sp -= 1;
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x04:		// INC B
			{
				inc_r(b);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x0C:		// INC C
			{
				inc_r(c);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x14:		// INC D
			{
				inc_r(d);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x1C:		// INC E
			{
				inc_r(e);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x24:		// INC H
			{
				inc_r(h);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x2C:		// INC L
			{
				inc_r(l);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x34:		// INC (HL)
			{
				u8 rhl = mem_read(hl);
				
				inc_r(rhl);
				
				mem_write(hl, rhl);
				
				pc += 1;
				t = 12;
				break;
			}
			
			case 0x3C:		// INC A
			{
				inc_r(a);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x05:		// DEC B
			{
				dec_r(b);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x0D:		// DEC C
			{
				dec_r(c);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x15:		// DEC D
			{
				dec_r(d);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x1D:		// DEC E
			{
				dec_r(e);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x25:		// DEC H
			{
				dec_r(h);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x2D:		// DEC L
			{
				dec_r(l);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x35:		// DEC (HL)
			{
				u8 rhl = mem_read(hl);
				
				dec_r(rhl);
				
				mem_write(hl, rhl);
				
				pc += 1;
				t = 12;
				break;
			}
			
			case 0x3D:		// DEC A
			{
				dec_r(a);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x06:		// LD B, u8
			{
				b = mem_read(pc + 1);
				
				pc += 2;
				t = 8;
				break;
			}
			
			case 0x0E:		// LD C, u8
			{
				c = mem_read(pc + 1);
				
				pc += 2;
				t = 8;
				break;
			}
			
			case 0x16:		// LD D, u8
			{
				d = mem_read(pc + 1);
				
				pc += 2;
				t = 8;
				break;
			}
			
			case 0x1E:		// LD E, u8
			{
				e = mem_read(pc + 1);
				
				pc += 2;
				t = 8;
				break;
			}
			
			case 0x26:		// LD H, u8
			{
				h = mem_read(pc + 1);
				
				pc += 2;
				t = 8;
				break;
			}
			
			case 0x2E:		// LD L, u8
			{
				l = mem_read(pc + 1);
				
				pc += 2;
				t = 8;
				break;
			}
			
			case 0x36:		// LD (HL), u8
			{
				mem_write(hl, mem_read(pc + 1));
				
				pc += 2;
				t = 12;
				break;
			}
			
			case 0x3E:		// LD A, u8
			{
				a = mem_read(pc + 1);
				
				pc += 2;
				t = 8;
				break;
			}
			
			case 0x07:		// RLCA
			{
				ca = (a >> 7) & 1;
				a <<= 1;
				a |= ca;
				
				z = 0;
				n = 0;
				hc = 0;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x0F:		// RRCA
			{
				ca = a & 1;
				a >>= 1;
				a |= ca << 7;
				
				z = 0;
				n = 0;
				hc = 0;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x17:		// RLA
			{
				int prev7 = (a >> 7) & 1;
				a <<= 1;
				a |= ca;
				ca = prev7;
				
				z = 0;
				n = 0;
				hc = 0;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x1F:		// RRA
			{
				int prev0 = a & 1;
				a >>= 1;
				a |= ca << 7;
				ca = prev0;
				
				z = 0;
				n = 0;
				hc = 0;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x27:		// DAA
			{
				if (n == 0)
				{
					if (ca || (a > 0x99))
					{
						ca = 1;
						
						a += 0x60;
					}
					
					if (hc || ((a & 0x0F) > 0x09))
					{
						a += 0x06;
					}
				}
				
				else
				{
					if (ca)
					{
						a -= 0x60;
					}
					
					if (hc)
					{
						a -= 0x06;
					}
				}
				
				z = (a == 0) ? 1 : 0;
				hc = 0;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x2F:		// CPL
			{
				a = ~a;
				
				n = 1;
				hc = 1;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x37:		// SCF
			{
				ca = 1;
				
				n = 0;
				hc = 0;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x3F:		// CCF
			{
				ca = !ca;
				
				n = 0;
				hc = 0;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x40:		// LD B, B
			{
				// do I really need to?
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x48:		// LD C, B
			{
				c = b;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x50:		// LD D, B
			{
				d = b;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x58:		// LD E, B
			{
				e = b;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x60:		// LD H, B
			{
				h = b;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x68:		// LD L, B
			{
				l = b;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x70:		// LD (HL), B
			{
				mem_write(hl, b);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x78:		// LD A, B
			{
				a = b;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x41:		// LD B, C
			{
				b = c;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x49:		// LD C, C
			{
				// seriously it's just a nop
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x51:		// LD D, C
			{
				d = c;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x59:		// LD E, C
			{
				e = c;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x61:		// LD H, C
			{
				h = c;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x69:		// LD L, C
			{
				l = c;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x71:		// LD (HL), C
			{
				mem_write(hl, c);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x79:		// LD A, C
			{
				a = c;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x42:		// LD B, D
			{
				b = d;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x4A:		// LD C, D
			{
				c = d;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x52:		// LD D, D
			{
				// ...dude don't tell me what to do
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x5A:		// LD E, D
			{
				e = d;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x62:		// LD H, D
			{
				h = d;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x6A:		// LD L, D
			{
				l = d;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x72:		// LD (HL), D
			{
				mem_write(hl, d);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x7A:		// LD A, D
			{
				a = d;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x43:		// LD B, E
			{
				b = e;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x4B:		// LD C, E
			{
				c = e;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x53:		// LD D, E
			{
				d = e;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x5B:		// LD E, E
			{
				// get off my back, man
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x63:		// LD H, E
			{
				h = e;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x6B:		// LD L, E
			{
				l = e;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x73:		// LD (HL), E
			{
				mem_write(hl, e);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x7B:		// LD A, E
			{
				a = e;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x44:		// LD B, H
			{
				b = h;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x4C:		// LD C, H
			{
				c = h;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x54:		// LD D, H
			{
				d = h;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x5C:		// LD E, H
			{
				e = h;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x64:		// LD H, H
			{
				// no, I'M batman
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x6C:		// LD L, H
			{
				l = h;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x74:		// LD (HL), H
			{
				mem_write(hl, h);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x7C:		// LD A, H
			{
				a = h;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x45:		// LD B, L
			{
				b = l;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x4D:		// LD C, L
			{
				c = l;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x55:		// LD D, L
			{
				d = l;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x5D:		// LD E, L
			{
				e = l;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x65:		// LD H, L
			{
				h = l;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x6D:		// LD L, L
			{
				// no, I didn't say...wait no I'M batman
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x75:		// LD (HL), L
			{
				mem_write(hl, l);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x7D:		// LD A, L
			{
				a = l;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x46:		// LD B, (HL)
			{
				b = mem_read(hl);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x4E:		// LD C, (HL)
			{
				c = mem_read(hl);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x56:		// LD D, (HL)
			{
				d = mem_read(hl);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x5E:		// LD E, (HL)
			{
				e = mem_read(hl);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x66:		// LD H, (HL)
			{
				h = mem_read(hl);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x6E:		// LD L, (HL)
			{
				l = mem_read(hl);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x76:		// HALT
			{
				halted = true;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x7E:		// LD A, (HL)
			{
				a = mem_read(hl);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x47:		// LD B, A
			{
				b = a;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x4F:		// LD C, A
			{
				c = a;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x57:		// LD D, A
			{
				d = a;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x5F:		// LD E, A
			{
				e = a;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x67:		// LD H, A
			{
				h = a;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x6F:		// LD L, A
			{
				l = a;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x77:		// LD (HL), A
			{
				mem_write(hl, a);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x7F:		// LD A, A
			{
				// ...I think I've written this emulator too many times...
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x80:		// ADD A, B
			{
				add_a(b);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x88:		// ADC A, B
			{
				adc_a(b);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x90:		// SUB A, B
			{
				sub_a(b);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x98:		// SBC A, B
			{
				sbc_a(b);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xA0:		// AND A, B
			{
				and_a(b);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xA8:		// XOR A, B
			{
				xor_a(b);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xB0:		// OR A, B
			{
				or_a(b);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xB8:		// CP A, B
			{
				cp_a(b);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x81:		// ADD A, C
			{
				add_a(c);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x89:		// ADC A, C
			{
				adc_a(c);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x91:		// SUB A, C
			{
				sub_a(c);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x99:		// SBC A, C
			{
				sbc_a(c);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xA1:		// AND A, C
			{
				and_a(c);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xA9:		// XOR A, C
			{
				xor_a(c);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xB1:		// OR A, C
			{
				or_a(c);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xB9:		// CP A, C
			{
				cp_a(c);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x82:		// ADD A, D
			{
				add_a(d);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x8A:		// ADC A, D
			{
				adc_a(d);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x92:		// SUB A, D
			{
				sub_a(d);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x9A:		// SBC A, D
			{
				sbc_a(d);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xA2:		// AND A, D
			{
				and_a(d);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xAA:		// XOR A, D
			{
				xor_a(d);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xB2:		// OR A, D
			{
				or_a(d);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xBA:		// CP A, D
			{
				cp_a(d);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x83:		// ADD A, E
			{
				add_a(e);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x8B:		// ADC A, E
			{
				adc_a(e);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x93:		// SUB A, E
			{
				sub_a(e);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x9B:		// SBC A, E
			{
				sbc_a(e);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xA3:		// AND A, E
			{
				and_a(e);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xAB:		// XOR A, E
			{
				xor_a(e);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xB3:		// OR A, E
			{
				or_a(e);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xBB:		// CP A, E
			{
				cp_a(e);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x84:		// ADD A, H
			{
				add_a(h);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x8C:		// ADC A, H
			{
				adc_a(h);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x94:		// SUB A, H
			{
				sub_a(h);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x9C:		// SBC A, H
			{
				sbc_a(h);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xA4:		// AND A, H
			{
				and_a(h);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xAC:		// XOR A, H
			{
				xor_a(h);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xB4:		// OR A, H
			{
				or_a(h);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xBC:		// CP A, H
			{
				cp_a(h);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x85:		// ADD A, L
			{
				add_a(l);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x8D:		// ADC A, L
			{
				adc_a(l);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x95:		// SUB A, L
			{
				sub_a(l);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x9D:		// SBC A, L
			{
				sbc_a(l);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xA5:		// AND A, L
			{
				and_a(l);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xAD:		// XOR A, L
			{
				xor_a(l);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xB5:		// OR A, L
			{
				or_a(l);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xBD:		// CP A, L
			{
				cp_a(l);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x86:		// ADD A, (HL)
			{
				add_a(mem_read(hl));
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x8E:		// ADC A, (HL)
			{
				adc_a(mem_read(hl));
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x96:		// SUB A, (HL)
			{
				sub_a(mem_read(hl));
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x9E:		// SBC A, (HL)
			{
				sbc_a(mem_read(hl));
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0xA6:		// AND A, (HL)
			{
				and_a(mem_read(hl));
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0xAE:		// XOR A, (HL)
			{
				xor_a(mem_read(hl));
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0xB6:		// OR A, (HL)
			{
				or_a(mem_read(hl));
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0xBE:		// CP A, (HL)
			{
				cp_a(mem_read(hl));
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0x87:		// ADD A, A
			{
				add_a(a);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x8F:		// ADC A, A
			{
				adc_a(a);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x97:		// SUB A, A
			{
				sub_a(a);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0x9F:		// SBC A, A
			{
				sbc_a(a);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xA7:		// AND A, A
			{
				and_a(a);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xAF:		// XOR A, A
			{
				xor_a(a);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xB7:		// OR A, A
			{
				or_a(a);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xBF:		// CP A, A
			{
				cp_a(a);
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xC0:		// RET NZ
			{
				if (z == 0)
				{
					ret();
					t = 20;
				}
				
				else
				{
					pc += 1;
					t = 8;
				}
				
				break;
			}
			
			case 0xC8:		// RET Z
			{
				if (z == 1)
				{
					ret();
					t = 20;
				}
				
				else
				{
					pc += 1;
					t = 8;
				}
				
				break;
			}
			
			case 0xD0:		// RET NC
			{
				if (ca == 0)
				{
					ret();
					t = 20;
				}
				
				else
				{
					pc += 1;
					t = 8;
				}
				
				break;
			}
			
			case 0xD8:		// RET C
			{
				if (ca == 1)
				{
					ret();
					t = 20;
				}
				
				else
				{
					pc += 1;
					t = 8;
				}
				
				break;
			}
			
			case 0xE0:		// LD (0xFF00 + u8), A
			{
				mem_write(0xFF00 + mem_read(pc + 1), a);
				
				pc += 2;
				t = 12;
				break;
			}
			
			case 0xE8:		// ADD SP, e8
			{
				add_sp((e8) (mem_read(pc + 1)));
				
				pc += 2;
				t = 16;
				break;
			}
			
			case 0xF0:		// LD A, (0xFF00 + u8)
			{
				a = mem_read(0xFF00 + mem_read(pc + 1));
				
				pc += 2;
				t = 12;
				break;
			}
			
			case 0xF8:		// LD HL, SP + e8
			{
				ld_hl_spd((e8) (mem_read(pc + 1)));
				
				pc += 2;
				t = 16;
				break;
			}
			
			case 0xC1:		// POP BC
			{
				bc = pop();
				
				pc += 1;
				t = 12;
				break;
			}
			
			case 0xC9:		// RET
			{
				ret();
				
				t = 16;
				break;
			}
			
			case 0xD1:		// POP DE
			{
				de = pop();
				
				pc += 1;
				t = 12;
				break;
			}
			
			case 0xD9:		// RETI
			{
				ime = true;
				ret();
				
				t = 16;
				break;
			}
			
			case 0xE1:		// POP HL
			{
				hl = pop();
				
				pc += 1;
				t = 12;
				break;
			}
			
			case 0xE9:		// JP HL
			{
				pc = hl;
				t = 4;
				break;
			}
			
			case 0xF1:		// POP AF
			{
				af = pop();
				
				updateFlags();
				
				pc += 1;
				t = 12;
				break;
			}
			
			case 0xF9:		// LD SP, HL
			{
				sp = hl;
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0xC2:		// JP NZ, u16
			{
				if (z == 0)
				{
					pc = op16;
					t = 16;
				}
				
				else
				{
					pc += 3;
					t = 12;
				}
				
				break;
			}
			
			case 0xCA:		// JP Z, u16
			{
				if (z == 1)
				{
					pc = op16;
					t = 16;
				}
				
				else
				{
					pc += 3;
					t = 12;
				}
				
				break;
			}
			
			case 0xD2:		// JP NC, u16
			{
				if (ca == 0)
				{
					pc = op16;
					t = 16;
				}
				
				else
				{
					pc += 3;
					t = 12;
				}
				
				break;
			}
			
			case 0xDA:		// JP C, u16
			{
				if (ca == 1)
				{
					pc = op16;
					t = 16;
				}
				
				else
				{
					pc += 3;
					t = 12;
				}
				
				break;
			}
			
			case 0xE2:		// LD (0xFF00 + C), A
			{
				mem_write(0xFF00 + c, a);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0xEA:		// LD (u16), A
			{
				mem_write(op16, a);
				
				pc += 3;
				t = 16;
				break;
			}
			
			case 0xF2:		// LD A, (0xFF00 + C)
			{
				a = mem_read(0xFF00 + c);
				
				pc += 1;
				t = 8;
				break;
			}
			
			case 0xFA:		// LD A, (u16)
			{
				a = mem_read(op16);
				
				pc += 3;
				t = 16;
				break;
			}
			
			case 0xC3:		// JP u16
			{
				pc = op16;
				
				t = 16;
				break;
			}
			
			case 0xCB:		// PREFIX CB
			{
				cb();
				
				pc += 2;
				break;
			}
			
			case 0xF3:		// DI
			{
				ime = false;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xFB:		// EI
			{
				ei_set = true;
				
				pc += 1;
				t = 4;
				break;
			}
			
			case 0xC4:		// CALL NZ, u16
			{
				if (z == 0)
				{
					call(op16);
					t = 24;
				}
				
				else
				{
					pc += 3;
					t = 12;
				}
				
				break;
			}
			
			case 0xCC:		// CALL Z, u16
			{
				if (z == 1)
				{
					call(op16);
					t = 24;
				}
				
				else
				{
					pc += 3;
					t = 12;
				}
				
				break;
			}
			
			case 0xD4:		// CALL NC, u16
			{
				if (ca == 0)
				{
					call(op16);
					t = 24;
				}
				
				else
				{
					pc += 3;
					t = 12;
				}
				
				break;
			}
			
			case 0xDC:		// CALL C, u16
			{
				if (ca == 1)
				{
					call(op16);
					t = 24;
				}
				
				else
				{
					pc += 3;
					t = 12;
				}
				
				break;
			}
			
			case 0xC5:		// PUSH BC
			{
				push(bc);
				
				pc += 1;
				t = 16;
				break;
			}
			
			case 0xCD:		// CALL u16
			{
				call(op16);
				
				t = 24;
				break;
			}
			
			case 0xD5:		// PUSH DE
			{
				push(de);
				
				pc += 1;
				t = 16;
				break;
			}
			
			case 0xE5:		// PUSH HL
			{
				push(hl);
				
				pc += 1;
				t = 16;
				break;
			}
			
			case 0xF5:		// PUSH AF
			{
				updateF();
				
				push(af);
				
				pc += 1;
				t = 16;
				break;
			}
			
			case 0xC6:		// ADD A, u8
			{
				add_a(mem_read(pc + 1));
				
				pc += 2;
				t = 8;
				break;
			}
			
			case 0xCE:		// ADC A, u8
			{
				adc_a(mem_read(pc + 1));
				
				pc += 2;
				t = 8;
				break;
			}
			
			case 0xD6:		// SUB A, u8
			{
				sub_a(mem_read(pc + 1));
				
				pc += 2;
				t = 8;
				break;
			}
			
			case 0xDE:		// SBC A, u8
			{
				sbc_a(mem_read(pc + 1));
				
				pc += 2;
				t = 8;
				break;
			}
			
			case 0xE6:		// AND A, u8
			{
				and_a(mem_read(pc + 1));
				
				pc += 2;
				t = 8;
				break;
			}
			
			case 0xEE:		// XOR A, u8
			{
				xor_a(mem_read(pc + 1));
				
				pc += 2;
				t = 8;
				break;
			}
			
			case 0xF6:		// OR A, u8
			{
				or_a(mem_read(pc + 1));
				
				pc += 2;
				t = 8;
				break;
			}
			
			case 0xFE:		// CP A, u8
			{
				cp_a(mem_read(pc + 1));
				
				pc += 2;
				t = 8;
				break;
			}
			
			case 0xC7:		// RST 0x00
			{
				rst(0x0000);
				
				t = 16;
				break;
			}
			
			case 0xCF:		// RST 0x08
			{
				rst(0x0008);
				
				t = 16;
				break;
			}
			
			case 0xD7:		// RST 0x10
			{
				rst(0x0010);
				
				t = 16;
				break;
			}
			
			case 0xDF:		// RST 0x18
			{
				rst(0x0018);
				
				t = 16;
				break;
			}
			
			case 0xE7:		// RST 0x20
			{
				rst(0x0000);
				
				t = 16;
				break;
			}
			
			case 0xEF:		// RST 0x28
			{
				rst(0x0028);
				
				t = 16;
				break;
			}
			
			case 0xF7:		// RST 0x30
			{
				rst(0x0030);
				
				t = 16;
				break;
			}
			
			case 0xFF:		// RST 0x38
			{
				rst(0x0038);
				
				t = 16;
				break;
			}
			
			default:
			{
				printf("invalid opcode 0x%02X at pc 0x%04X\n", instr, pc);
				exit(1);
				break;
			}
		}
	}
	
	if (mem[IF] != 0)
	{
		if ((mem[IE] & 0b00001) != 0 && (mem[IF] & 0b00001) != 0)
		{
			halted = false;
			
			if (ime)
			{
				mem[IF] &= 0b11110;
				
				intr(0x0040);
			}
		}
		
		else if ((mem[IE] & 0b00010) != 0 && (mem[IF] & 0b00010) != 0)
		{
			halted = false;
			
			if (ime)
			{
				mem[IF] &= 0b11101;
				
				intr(0x0048);
			}
		}
		
		else if ((mem[IE] & 0b00100) != 0 && (mem[IF] & 0b00100) != 0)
		{
			halted = false;
			
			if (ime)
			{
				mem[IF] &= 0b11011;
				
				intr(0x0050);
			}
		}
		
		else if ((mem[IE] & 0b01000) != 0 && (mem[IF] & 0b01000) != 0)
		{
			halted = false;
			
			if (ime)
			{
				mem[IF] &= 0b10111;
				
				intr(0x0058);
			}
		}
		
		else if ((mem[IE] & 0b10000) != 0 && (mem[IF] & 0b10000) != 0)
		{
			halted = false;
			
			if (ime)
			{
				mem[IF] &= 0b01111;
				
				intr(0x0060);
			}
		}
	}
}



inline void updateF()
{
	f = (z << 7) | (n << 6) | (hc << 5) | (ca << 4);
}

inline void updateFlags()
{
	z = (f >> 7) & 1;
	n = (f >> 6) & 1;
	hc = (f >> 5) & 1;
	ca = (f >> 4) & 1;
}



inline void add_hl(u16 operand)
{
	int flags = hl ^ operand ^ (hl + operand);
	
	hl += operand;
	
	n = 0;
	
	hc = (flags & 0x1000) ? 1 : 0;
	ca = (flags & 0x10000) ? 1 : 0;
}

inline void add_sp(e8 operand)
{
	int flags = sp ^ operand ^ (sp + operand);
	
	sp += operand;
	
	z = 0;
	n = 0;
	
	// CONTROVERSIAL
	hc = (flags & 0x0010) ? 1 : 0;
	ca = (flags & 0x0100) ? 1 : 0;
}

inline void ld_hl_spd(e8 operand)
{
	hl = sp + operand;
	
	int flags = sp ^ operand ^ (sp + operand);
	
	z = 0;
	n = 0;
	
	// CONTROVERSIAL
	hc = (flags & 0x0010) ? 1 : 0;
	ca = (flags & 0x0100) ? 1 : 0;
}

inline void call(u16 addr)
{
	push(pc + 3);
	
	pc = addr;
}

inline void ret()
{
	pc = pop();
}

inline void rst(int vec)
{
	push(pc + 1);
	
	pc = vec;
}

inline void intr(int vec)
{
	ime = false;
	
	push(pc);
	
	pc = vec;
	
	t += 20;
}

inline void push(u16 value)
{
	sp -= 2;
	
	mem_write(sp, value & 0xFF);
	mem_write(sp + 1, value >> 8);
}

inline u16 pop()
{
	u16 value = (mem_read(sp + 1) << 8) | mem_read(sp);
	
	sp += 2;
	
	return value;
}



inline void add_a(u8 operand)
{
	int flags = a ^ operand ^ (a + operand);
	
	a += operand;
	
	z = a == 0 ? 1 : 0;
	n = 0;
	
	hc = (flags & 0x10) != 0 ? 1 : 0;
	ca = (flags & 0x100) != 0 ? 1 : 0;
}

inline void adc_a(u8 operand)
{
	int flags = a ^ operand ^ (a + operand + ca);
	
	a += operand + ca;
	
	z = a == 0 ? 1 : 0;
	n = 0;
	
	hc = (flags & 0x10) != 0 ? 1 : 0;
	ca = (flags & 0x100) != 0 ? 1 : 0;
}

inline void sub_a(u8 operand)
{
	hc = (a & 0xF) < (operand & 0xF) ? 1 : 0;
	ca = a < operand ? 1 : 0;
	
	a -= operand;
	
	z = (a == 0) ? 1 : 0;
	n = 1;
}

inline void sbc_a(u8 operand)
{
	int prevCA = ca;
	
	hc = ((a & 0xF) < ((operand & 0xF) + ca)) ? 1 : 0;
	ca = (a < (operand + ca)) ? 1 : 0;
	
	a -= operand + prevCA;
	
	z = (a == 0) ? 1 : 0;
	n = 1;
}

inline void and_a(u8 operand)
{
	a &= operand;
	
	z = (a == 0) ? 1 : 0;
	n = 0;
	hc = 1;
	ca = 0;
}

inline void xor_a(u8 operand)
{
	a ^= operand;
	
	z = (a == 0) ? 1 : 0;
	n = 0;
	hc = 0;
	ca = 0;
}

inline void or_a(u8 operand)
{
	a |= operand;
	
	z = (a == 0) ? 1 : 0;
	n = 0;
	hc = 0;
	ca = 0;
}

inline void cp_a(u8 operand)
{
	hc = ((a & 0xF) < (operand & 0xF)) ? 1 : 0;
	ca = (a < operand) ? 1 : 0;
	
	int result = a - operand;
	
	z = (result == 0) ? 1 : 0;
	n = 1;
}



inline void cb()
{
	// begin cb nightmare
	
	switch (mem_read(pc + 1))
	{
		case 0x00:		// RLC B
		{
			rlc_r(b);
			
			t = 8;
			break;
		}
		
		case 0x08:		// RRC B
		{
			rrc_r(b);
			
			t = 8;
			break;
		}
		
		case 0x10:		// RL B
		{
			rl_r(b);
			
			t = 8;
			break;
		}
		
		case 0x18:		// RR B
		{
			rr_r(b);
			
			t = 8;
			break;
		}
		
		case 0x20:		// SLA B
		{
			sla_r(b);
			
			t = 8;
			break;
		}
		
		case 0x28:		// SRA B
		{
			sra_r(b);
			
			t = 8;
			break;
		}
		
		case 0x30:		// SWAP B
		{
			swap_r(b);
			
			t = 8;
			break;
		}
		
		case 0x38:		// SRL B
		{
			srl_r(b);
			
			t = 8;
			break;
		}
		
		case 0x01:		// RLC C
		{
			rlc_r(c);
			
			t = 8;
			break;
		}
		
		case 0x09:		// RRC C
		{
			rrc_r(c);
			
			t = 8;
			break;
		}
		
		case 0x11:		// RL C
		{
			rl_r(c);
			
			t = 8;
			break;
		}
		
		case 0x19:		// RR C
		{
			rr_r(c);
			
			t = 8;
			break;
		}
		
		case 0x21:		// SLA C
		{
			sla_r(c);
			
			t = 8;
			break;
		}
		
		case 0x29:		// SRA C
		{
			sra_r(c);
			
			t = 8;
			break;
		}
		
		case 0x31:		// SWAP C
		{
			swap_r(c);
			
			t = 8;
			break;
		}
		
		case 0x39:		// SRL C
		{
			srl_r(c);
			
			t = 8;
			break;
		}
		
		case 0x02:		// RLC D
		{
			rlc_r(d);
			
			t = 8;
			break;
		}
		
		case 0x0A:		// RRC D
		{
			rrc_r(d);
			
			t = 8;
			break;
		}
		
		case 0x12:		// RL D
		{
			rl_r(d);
			
			t = 8;
			break;
		}
		
		case 0x1A:		// RR D
		{
			rr_r(d);
			
			t = 8;
			break;
		}
		
		case 0x22:		// SLA D
		{
			sla_r(d);
			
			t = 8;
			break;
		}
		
		case 0x2A:		// SRA D
		{
			sra_r(d);
			
			t = 8;
			break;
		}
		
		case 0x32:		// SWAP D
		{
			swap_r(d);
			
			t = 8;
			break;
		}
		
		case 0x3A:		// SRL D
		{
			srl_r(d);
			
			t = 8;
			break;
		}
		
		case 0x03:		// RLC E
		{
			rlc_r(e);
			
			t = 8;
			break;
		}
		
		case 0x0B:		// RRC E
		{
			rrc_r(e);
			
			t = 8;
			break;
		}
		
		case 0x13:		// RL E
		{
			rl_r(e);
			
			t = 8;
			break;
		}
		
		case 0x1B:		// RR E
		{
			rr_r(e);
			
			t = 8;
			break;
		}
		
		case 0x23:		// SLA E
		{
			sla_r(e);
			
			t = 8;
			break;
		}
		
		case 0x2B:		// SRA E
		{
			sra_r(e);
			
			t = 8;
			break;
		}
		
		case 0x33:		// SWAP E
		{
			swap_r(e);
			
			t = 8;
			break;
		}
		
		case 0x3B:		// SRL E
		{
			srl_r(e);
			
			t = 8;
			break;
		}
		
		case 0x04:		// RLC H
		{
			rlc_r(h);
			
			t = 8;
			break;
		}
		
		case 0x0C:		// RRC H
		{
			rrc_r(h);
			
			t = 8;
			break;
		}
		
		case 0x14:		// RL H
		{
			rl_r(h);
			
			t = 8;
			break;
		}
		
		case 0x1C:		// RR H
		{
			rr_r(h);
			
			t = 8;
			break;
		}
		
		case 0x24:		// SLA H
		{
			sla_r(h);
			
			t = 8;
			break;
		}
		
		case 0x2C:		// SRA H
		{
			sra_r(h);
			
			t = 8;
			break;
		}
		
		case 0x34:		// SWAP H
		{
			swap_r(h);
			
			t = 8;
			break;
		}
		
		case 0x3C:		// SRL H
		{
			srl_r(h);
			
			t = 8;
			break;
		}
		
		case 0x05:		// RLC L
		{
			rlc_r(l);
			
			t = 8;
			break;
		}
		
		case 0x0D:		// RRC L
		{
			rrc_r(l);
			
			t = 8;
			break;
		}
		
		case 0x15:		// RL L
		{
			rl_r(l);
			
			t = 8;
			break;
		}
		
		case 0x1D:		// RR L
		{
			rr_r(l);
			
			t = 8;
			break;
		}
		
		case 0x25:		// SLA L
		{
			sla_r(l);
			
			t = 8;
			break;
		}
		
		case 0x2D:		// SRA L
		{
			sra_r(l);
			
			t = 8;
			break;
		}
		
		case 0x35:		// SWAP L
		{
			swap_r(l);
			
			t = 8;
			break;
		}
		
		case 0x3D:		// SRL L
		{
			srl_r(l);
			
			t = 8;
			break;
		}
		
		case 0x06:		// RLC (HL)
		{
			u8 rhl = mem_read(hl);
			
			rlc_r(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0x0E:		// RRC (HL)
		{
			u8 rhl = mem_read(hl);
			
			rrc_r(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0x16:		// RL (HL)
		{
			u8 rhl = mem_read(hl);
			
			rl_r(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0x1E:		// RR (HL)
		{
			u8 rhl = mem_read(hl);
			
			rr_r(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0x26:		// SLA (HL)
		{
			u8 rhl = mem_read(hl);
			
			sla_r(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0x2E:		// SRA (HL)
		{
			u8 rhl = mem_read(hl);
			
			sra_r(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0x36:		// SWAP (HL)
		{
			u8 rhl = mem_read(hl);
			
			swap_r(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0x3E:		// SRL (HL)
		{
			u8 rhl = mem_read(hl);
			
			srl_r(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0x07:		// RLC A
		{
			rlc_r(a);
			
			t = 8;
			break;
		}
		
		case 0x0F:		// RRC A
		{
			rrc_r(a);
			
			t = 8;
			break;
		}
		
		case 0x17:		// RL A
		{
			rl_r(a);
			
			t = 8;
			break;
		}
		
		case 0x1F:		// RR A
		{
			rr_r(a);
			
			t = 8;
			break;
		}
		
		case 0x27:		// SLA A
		{
			sla_r(a);
			
			t = 8;
			break;
		}
		
		case 0x2F:		// SRA A
		{
			sra_r(a);
			
			t = 8;
			break;
		}
		
		case 0x37:		// SWAP A
		{
			swap_r(a);
			
			t = 8;
			break;
		}
		
		case 0x3F:		// SRL A
		{
			srl_r(a);
			
			t = 8;
			break;
		}
		
		case 0x40:		// BIT 0, B
		{
			bit0(b);
			
			t = 8;
			break;
		}
		
		case 0x48:		// BIT 1, B
		{
			bit1(b);
			
			t = 8;
			break;
		}
		
		case 0x50:		// BIT 2, B
		{
			bit2(b);
			
			t = 8;
			break;
		}
		
		case 0x58:		// BIT 3, B
		{
			bit3(b);
			
			t = 8;
			break;
		}
		
		case 0x60:		// BIT 4, B
		{
			bit4(b);
			
			t = 8;
			break;
		}
		
		case 0x68:		// BIT 5, B
		{
			bit5(b);
			
			t = 8;
			break;
		}
		
		case 0x70:		// BIT 6, B
		{
			bit6(b);
			
			t = 8;
			break;
		}
		
		case 0x78:		// BIT 7, B
		{
			bit7(b);
			
			t = 8;
			break;
		}
		
		case 0x41:		// BIT 0, C
		{
			bit0(c);
			
			t = 8;
			break;
		}
		
		case 0x49:		// BIT 1, C
		{
			bit1(c);
			
			t = 8;
			break;
		}
		
		case 0x51:		// BIT 2, C
		{
			bit2(c);
			
			t = 8;
			break;
		}
		
		case 0x59:		// BIT 3, C
		{
			bit3(c);
			
			t = 8;
			break;
		}
		
		case 0x61:		// BIT 4, C
		{
			bit4(c);
			
			t = 8;
			break;
		}
		
		case 0x69:		// BIT 5, C
		{
			bit5(c);
			
			t = 8;
			break;
		}
		
		case 0x71:		// BIT 6, C
		{
			bit6(c);
			
			t = 8;
			break;
		}
		
		case 0x79:		// BIT 7, C
		{
			bit7(c);
			
			t = 8;
			break;
		}
		
		case 0x42:		// BIT 0, D
		{
			bit0(d);
			
			t = 8;
			break;
		}
		
		case 0x4A:		// BIT 1, D
		{
			bit1(d);
			
			t = 8;
			break;
		}
		
		case 0x52:		// BIT 2, D
		{
			bit2(d);
			
			t = 8;
			break;
		}
		
		case 0x5A:		// BIT 3, D
		{
			bit3(d);
			
			t = 8;
			break;
		}
		
		case 0x62:		// BIT 4, D
		{
			bit4(d);
			
			t = 8;
			break;
		}
		
		case 0x6A:		// BIT 5, D
		{
			bit5(d);
			
			t = 8;
			break;
		}
		
		case 0x72:		// BIT 6, D
		{
			bit6(d);
			
			t = 8;
			break;
		}
		
		case 0x7A:		// BIT 7, D
		{
			bit7(d);
			
			t = 8;
			break;
		}
		
		case 0x43:		// BIT 0, E
		{
			bit0(e);
			
			t = 8;
			break;
		}
		
		case 0x4B:		// BIT 1, E
		{
			bit1(e);
			
			t = 8;
			break;
		}
		
		case 0x53:		// BIT 2, E
		{
			bit2(e);
			
			t = 8;
			break;
		}
		
		case 0x5B:		// BIT 3, E
		{
			bit3(e);
			
			t = 8;
			break;
		}
		
		case 0x63:		// BIT 4, E
		{
			bit4(e);
			
			t = 8;
			break;
		}
		
		case 0x6B:		// BIT 5, E
		{
			bit5(e);
			
			t = 8;
			break;
		}
		
		case 0x73:		// BIT 6, E
		{
			bit6(e);
			
			t = 8;
			break;
		}
		
		case 0x7B:		// BIT 7, E
		{
			bit7(e);
			
			t = 8;
			break;
		}
		
		case 0x44:		// BIT 0, H
		{
			bit0(h);
			
			t = 8;
			break;
		}
		
		case 0x4C:		// BIT 1, H
		{
			bit1(h);
			
			t = 8;
			break;
		}
		
		case 0x54:		// BIT 2, H
		{
			bit2(h);
			
			t = 8;
			break;
		}
		
		case 0x5C:		// BIT 3, H
		{
			bit3(h);
			
			t = 8;
			break;
		}
		
		case 0x64:		// BIT 4, H
		{
			bit4(h);
			
			t = 8;
			break;
		}
		
		case 0x6C:		// BIT 5, H
		{
			bit5(h);
			
			t = 8;
			break;
		}
		
		case 0x74:		// BIT 6, H
		{
			bit6(h);
			
			t = 8;
			break;
		}
		
		case 0x7C:		// BIT 7, H
		{
			bit7(h);
			
			t = 8;
			break;
		}
		
		case 0x45:		// BIT 0, L
		{
			bit0(l);
			
			t = 8;
			break;
		}
		
		case 0x4D:		// BIT 1, L
		{
			bit1(l);
			
			t = 8;
			break;
		}
		
		case 0x55:		// BIT 2, L
		{
			bit2(l);
			
			t = 8;
			break;
		}
		
		case 0x5D:		// BIT 3, L
		{
			bit3(l);
			
			t = 8;
			break;
		}
		
		case 0x65:		// BIT 4, L
		{
			bit4(l);
			
			t = 8;
			break;
		}
		
		case 0x6D:		// BIT 5, L
		{
			bit5(l);
			
			t = 8;
			break;
		}
		
		case 0x75:		// BIT 6, L
		{
			bit6(l);
			
			t = 8;
			break;
		}
		
		case 0x7D:		// BIT 7, L
		{
			bit7(l);
			
			t = 8;
			break;
		}
		
		case 0x46:		// BIT 0, (HL)
		{
			bit0(mem_read(hl));
			
			t = 12;
			break;
		}
		
		case 0x4E:		// BIT 1, (HL)
		{
			bit1(mem_read(hl));
			
			t = 12;
			break;
		}
		
		case 0x56:		// BIT 2, (HL)
		{
			bit2(mem_read(hl));
			
			t = 12;
			break;
		}
		
		case 0x5E:		// BIT 3, (HL)
		{
			bit3(mem_read(hl));
			
			t = 12;
			break;
		}
		
		case 0x66:		// BIT 4, (HL)
		{
			bit4(mem_read(hl));
			
			t = 12;
			break;
		}
		
		case 0x6E:		// BIT 5, (HL)
		{
			bit5(mem_read(hl));
			
			t = 12;
			break;
		}
		
		case 0x76:		// BIT 6, (HL)
		{
			bit6(mem_read(hl));
			
			t = 12;
			break;
		}
		
		case 0x7E:		// BIT 7, (HL)
		{
			bit7(mem_read(hl));
			
			t = 12;
			break;
		}
		
		case 0x47:		// BIT 0, A
		{
			bit0(a);
			
			t = 8;
			break;
		}
		
		case 0x4F:		// BIT 1, A
		{
			bit1(a);
			
			t = 8;
			break;
		}
		
		case 0x57:		// BIT 2, A
		{
			bit2(a);
			
			t = 8;
			break;
		}
		
		case 0x5F:		// BIT 3, A
		{
			bit3(a);
			
			t = 8;
			break;
		}
		
		case 0x67:		// BIT 4, A
		{
			bit4(a);
			
			t = 8;
			break;
		}
		
		case 0x6F:		// BIT 5, A
		{
			bit5(a);
			
			t = 8;
			break;
		}
		
		case 0x77:		// BIT 6, A
		{
			bit6(a);
			
			t = 8;
			break;
		}
		
		case 0x7F:		// BIT 7, A
		{
			bit7(a);
			
			t = 8;
			break;
		}
		
		case 0x80:		// RES 0, B
		{
			res0(b);
			
			t = 8;
			break;
		}
		
		case 0x88:		// RES 1, B
		{
			res1(b);
			
			t = 8;
			break;
		}
		
		case 0x90:		// RES 2, B
		{
			res2(b);
			
			t = 8;
			break;
		}
		
		case 0x98:		// RES 3, B
		{
			res3(b);
			
			t = 8;
			break;
		}
		
		case 0xA0:		// RES 4, B
		{
			res4(b);
			
			t = 8;
			break;
		}
		
		case 0xA8:		// RES 5, B
		{
			res5(b);
			
			t = 8;
			break;
		}
		
		case 0xB0:		// RES 6, B
		{
			res6(b);
			
			t = 8;
			break;
		}
		
		case 0xB8:		// RES 7, B
		{
			res7(b);
			
			t = 8;
			break;
		}
		
		case 0x81:		// RES 0, C
		{
			res0(c);
			
			t = 8;
			break;
		}
		
		case 0x89:		// RES 1, C
		{
			res1(c);
			
			t = 8;
			break;
		}
		
		case 0x91:		// RES 2, C
		{
			res2(c);
			
			t = 8;
			break;
		}
		
		case 0x99:		// RES 3, C
		{
			res3(c);
			
			t = 8;
			break;
		}
		
		case 0xA1:		// RES 4, C
		{
			res4(c);
			
			t = 8;
			break;
		}
		
		case 0xA9:		// RES 5, C
		{
			res5(c);
			
			t = 8;
			break;
		}
		
		case 0xB1:		// RES 6, C
		{
			res6(c);
			
			t = 8;
			break;
		}
		
		case 0xB9:		// RES 7, C
		{
			res7(c);
			
			t = 8;
			break;
		}
		
		case 0x82:		// RES 0, D
		{
			res0(d);
			
			t = 8;
			break;
		}
		
		case 0x8A:		// RES 1, D
		{
			res1(d);
			
			t = 8;
			break;
		}
		
		case 0x92:		// RES 2, D
		{
			res2(d);
			
			t = 8;
			break;
		}
		
		case 0x9A:		// RES 3, D
		{
			res3(d);
			
			t = 8;
			break;
		}
		
		case 0xA2:		// RES 4, D
		{
			res4(d);
			
			t = 8;
			break;
		}
		
		case 0xAA:		// RES 5, D
		{
			res5(d);
			
			t = 8;
			break;
		}
		
		case 0xB2:		// RES 6, D
		{
			res6(d);
			
			t = 8;
			break;
		}
		
		case 0xBA:		// RES 7, D
		{
			res7(d);
			
			t = 8;
			break;
		}
		
		case 0x83:		// RES 0, E
		{
			res0(e);
			
			t = 8;
			break;
		}
		
		case 0x8B:		// RES 1, E
		{
			res1(e);
			
			t = 8;
			break;
		}
		
		case 0x93:		// RES 2, E
		{
			res2(e);
			
			t = 8;
			break;
		}
		
		case 0x9B:		// RES 3, E
		{
			res3(e);
			
			t = 8;
			break;
		}
		
		case 0xA3:		// RES 4, E
		{
			res4(e);
			
			t = 8;
			break;
		}
		
		case 0xAB:		// RES 5, E
		{
			res5(e);
			
			t = 8;
			break;
		}
		
		case 0xB3:		// RES 6, E
		{
			res6(e);
			
			t = 8;
			break;
		}
		
		case 0xBB:		// RES 7, E
		{
			res7(e);
			
			t = 8;
			break;
		}
		
		case 0x84:		// RES 0, H
		{
			res0(h);
			
			t = 8;
			break;
		}
		
		case 0x8C:		// RES 1, H
		{
			res1(h);
			
			t = 8;
			break;
		}
		
		case 0x94:		// RES 2, H
		{
			res2(h);
			
			t = 8;
			break;
		}
		
		case 0x9C:		// RES 3, H
		{
			res3(h);
			
			t = 8;
			break;
		}
		
		case 0xA4:		// RES 4, H
		{
			res4(h);
			
			t = 8;
			break;
		}
		
		case 0xAC:		// RES 5, H
		{
			res5(h);
			
			t = 8;
			break;
		}
		
		case 0xB4:		// RES 6, H
		{
			res6(h);
			
			t = 8;
			break;
		}
		
		case 0xBC:		// RES 7, H
		{
			res7(h);
			
			t = 8;
			break;
		}
		
		case 0x85:		// RES 0, L
		{
			res0(l);
			
			t = 8;
			break;
		}
		
		case 0x8D:		// RES 1, L
		{
			res1(l);
			
			t = 8;
			break;
		}
		
		case 0x95:		// RES 2, L
		{
			res2(l);
			
			t = 8;
			break;
		}
		
		case 0x9D:		// RES 3, L
		{
			res3(l);
			
			t = 8;
			break;
		}
		
		case 0xA5:		// RES 4, L
		{
			res4(l);
			
			t = 8;
			break;
		}
		
		case 0xAD:		// RES 5, L
		{
			res5(l);
			
			t = 8;
			break;
		}
		
		case 0xB5:		// RES 6, L
		{
			res6(l);
			
			t = 8;
			break;
		}
		
		case 0xBD:		// RES 7, L
		{
			res7(l);
			
			t = 8;
			break;
		}
		
		case 0x86:		// RES 0, (HL)
		{
			u8 rhl = mem_read(hl);
			
			res0(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0x8E:		// RES 1, (HL)
		{
			u8 rhl = mem_read(hl);
			
			res1(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0x96:		// RES 2, (HL)
		{
			u8 rhl = mem_read(hl);
			
			res2(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0x9E:		// RES 3, (HL)
		{
			u8 rhl = mem_read(hl);
			
			res3(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0xA6:		// RES 4, (HL)
		{
			u8 rhl = mem_read(hl);
			
			res4(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0xAE:		// RES 5, (HL)
		{
			u8 rhl = mem_read(hl);
			
			res5(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0xB6:		// RES 6, (HL)
		{
			u8 rhl = mem_read(hl);
			
			res6(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0xBE:		// RES 7, (HL)
		{
			u8 rhl = mem_read(hl);
			
			res7(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0x87:		// RES 0, A
		{
			res0(a);
			
			t = 8;
			break;
		}
		
		case 0x8F:		// RES 1, A
		{
			res1(a);
			
			t = 8;
			break;
		}
		
		case 0x97:		// RES 2, A
		{
			res2(a);
			
			t = 8;
			break;
		}
		
		case 0x9F:		// RES 3, A
		{
			res3(a);
			
			t = 8;
			break;
		}
		
		case 0xA7:		// RES 4, A
		{
			res4(a);
			
			t = 8;
			break;
		}
		
		case 0xAF:		// RES 5, A
		{
			res5(a);
			
			t = 8;
			break;
		}
		
		case 0xB7:		// RES 6, A
		{
			res6(a);
			
			t = 8;
			break;
		}
		
		case 0xBF:		// RES 7, A
		{
			res7(a);
			
			t = 8;
			break;
		}
		
		case 0xC0:		// SET 0, B
		{
			set0(b);
			
			t = 8;
			break;
		}
		
		case 0xC8:		// SET 1, B
		{
			set1(b);
			
			t = 8;
			break;
		}
		
		case 0xD0:		// SET 2, B
		{
			set2(b);
			
			t = 8;
			break;
		}
		
		case 0xD8:		// SET 3, B
		{
			set3(b);
			
			t = 8;
			break;
		}
		
		case 0xE0:		// SET 4, B
		{
			set4(b);
			
			t = 8;
			break;
		}
		
		case 0xE8:		// SET 5, B
		{
			set5(b);
			
			t = 8;
			break;
		}
		
		case 0xF0:		// SET 6, B
		{
			set6(b);
			
			t = 8;
			break;
		}
		
		case 0xF8:		// SET 7, B
		{
			set7(b);
			
			t = 8;
			break;
		}
		
		case 0xC1:		// SET 0, C
		{
			set0(c);
			
			t = 8;
			break;
		}
		
		case 0xC9:		// SET 1, C
		{
			set1(c);
			
			t = 8;
			break;
		}
		
		case 0xD1:		// SET 2, C
		{
			set2(c);
			
			t = 8;
			break;
		}
		
		case 0xD9:		// SET 3, C
		{
			set3(c);
			
			t = 8;
			break;
		}
		
		case 0xE1:		// SET 4, C
		{
			set4(c);
			
			t = 8;
			break;
		}
		
		case 0xE9:		// SET 5, C
		{
			set5(c);
			
			t = 8;
			break;
		}
		
		case 0xF1:		// SET 6, C
		{
			set6(c);
			
			t = 8;
			break;
		}
		
		case 0xF9:		// SET 7, C
		{
			set7(c);
			
			t = 8;
			break;
		}
		
		case 0xC2:		// SET 0, D
		{
			set0(d);
			
			t = 8;
			break;
		}
		
		case 0xCA:		// SET 1, D
		{
			set1(d);
			
			t = 8;
			break;
		}
		
		case 0xD2:		// SET 2, D
		{
			set2(d);
			
			t = 8;
			break;
		}
		
		case 0xDA:		// SET 3, D
		{
			set3(d);
			
			t = 8;
			break;
		}
		
		case 0xE2:		// SET 4, D
		{
			set4(d);
			
			t = 8;
			break;
		}
		
		case 0xEA:		// SET 5, D
		{
			set5(d);
			
			t = 8;
			break;
		}
		
		case 0xF2:		// SET 6, D
		{
			set6(d);
			
			t = 8;
			break;
		}
		
		case 0xFA:		// SET 7, D
		{
			set7(d);
			
			t = 8;
			break;
		}
		
		case 0xC3:		// SET 0, E
		{
			set0(e);
			
			t = 8;
			break;
		}
		
		case 0xCB:		// SET 1, E
		{
			set1(e);
			
			t = 8;
			break;
		}
		
		case 0xD3:		// SET 2, E
		{
			set2(e);
			
			t = 8;
			break;
		}
		
		case 0xDB:		// SET 3, E
		{
			set3(e);
			
			t = 8;
			break;
		}
		
		case 0xE3:		// SET 4, E
		{
			set4(e);
			
			t = 8;
			break;
		}
		
		case 0xEB:		// SET 5, E
		{
			set5(e);
			
			t = 8;
			break;
		}
		
		case 0xF3:		// SET 6, E
		{
			set6(e);
			
			t = 8;
			break;
		}
		
		case 0xFB:		// SET 7, E
		{
			set7(e);
			
			t = 8;
			break;
		}
		
		case 0xC4:		// SET 0, H
		{
			set0(h);
			
			t = 8;
			break;
		}
		
		case 0xCC:		// SET 1, H
		{
			set1(h);
			
			t = 8;
			break;
		}
		
		case 0xD4:		// SET 2, H
		{
			set2(h);
			
			t = 8;
			break;
		}
		
		case 0xDC:		// SET 3, H
		{
			set3(h);
			
			t = 8;
			break;
		}
		
		case 0xE4:		// SET 4, H
		{
			set4(h);
			
			t = 8;
			break;
		}
		
		case 0xEC:		// SET 5, H
		{
			set5(h);
			
			t = 8;
			break;
		}
		
		case 0xF4:		// SET 6, H
		{
			set6(h);
			
			t = 8;
			break;
		}
		
		case 0xFC:		// SET 7, H
		{
			set7(h);
			
			t = 8;
			break;
		}
		
		case 0xC5:		// SET 0, L
		{
			set0(l);
			
			t = 8;
			break;
		}
		
		case 0xCD:		// SET 1, L
		{
			set1(l);
			
			t = 8;
			break;
		}
		
		case 0xD5:		// SET 2, L
		{
			set2(l);
			
			t = 8;
			break;
		}
		
		case 0xDD:		// SET 3, L
		{
			set3(l);
			
			t = 8;
			break;
		}
		
		case 0xE5:		// SET 4, L
		{
			set4(l);
			
			t = 8;
			break;
		}
		
		case 0xED:		// SET 5, L
		{
			set5(l);
			
			t = 8;
			break;
		}
		
		case 0xF5:		// SET 6, L
		{
			set6(l);
			
			t = 8;
			break;
		}
		
		case 0xFD:		// SET 7, L
		{
			set7(l);
			
			t = 8;
			break;
		}
		
		case 0xC6:		// SET 0, (HL)
		{
			u8 rhl = mem_read(hl);
			
			set0(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0xCE:		// SET 1, (HL)
		{
			u8 rhl = mem_read(hl);
			
			set1(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0xD6:		// SET 2, (HL)
		{
			u8 rhl = mem_read(hl);
			
			set2(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0xDE:		// SET 3, (HL)
		{
			u8 rhl = mem_read(hl);
			
			set3(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0xE6:		// SET 4, (HL)
		{
			u8 rhl = mem_read(hl);
			
			set4(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0xEE:		// SET 5, (HL)
		{
			u8 rhl = mem_read(hl);
			
			set5(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0xF6:		// SET 6, (HL)
		{
			u8 rhl = mem_read(hl);
			
			set6(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0xFE:		// SET 7, (HL)
		{
			u8 rhl = mem_read(hl);
			
			set7(rhl);
			
			mem_write(hl, rhl);
			
			t = 16;
			break;
		}
		
		case 0xC7:		// SET 0, A
		{
			set0(a);
			
			t = 8;
			break;
		}
		
		case 0xCF:		// SET 1, A
		{
			set1(a);
			
			t = 8;
			break;
		}
		
		case 0xD7:		// SET 2, A
		{
			set2(a);
			
			t = 8;
			break;
		}
		
		case 0xDF:		// SET 3, A
		{
			set3(a);
			
			t = 8;
			break;
		}
		
		case 0xE7:		// SET 4, A
		{
			set4(a);
			
			t = 8;
			break;
		}
		
		case 0xEF:		// SET 5, A
		{
			set5(a);
			
			t = 8;
			break;
		}
		
		case 0xF7:		// SET 6, A
		{
			set6(a);
			
			t = 8;
			break;
		}
		
		case 0xFF:		// SET 7, A
		{
			set7(a);
			
			t = 8;
			break;
		}
		
		default:
		{
			printf("invalid CB-prefixed opcode 0x%02X at pc 0x%04X\n", mem_read(pc + 1), pc);
			exit(1);
			break;
		}
	}
}