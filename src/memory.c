#include <memory.h>
#include <timer.h>
#include <ppu.h>

#include <time.h>

u8 mem[0x10000];

inline void init_mem()
{
	if (use_bios)
	{
		srand(time(NULL));
		
		for (int i = 0; i < 0x10000; ++i)
		{
			mem[i] = rand() * 255;
		}
	}
	
	else
	{
		for (int i = 0; i < 0x10000; ++i)
		{
			mem[i] = 0;
		}
		
		mem[BGP] = 0xE4;
	}
}

inline u8 mem_read(int addr)
{
	if (addr < 0x100 && use_bios)
	{
		return bios[addr];
	}
	
	switch (addr)
	{
		case DIV:
		{
			return intern >> 8;
		}
		
		case LY:
		{
			return ly;
		}
	}
	
	return mem[addr];
}

inline void mem_write(u16 addr, u8 value)
{
	if (addr < 0x8000)
	{
		return;
	}
	
	switch (addr)
	{
		case LCDC:
		{
			// TODO: update ppu internal registers
			//       then ppu won't read and shift every time
			break;
		}
		
		case DIV:
		{
			mem[addr] = 0;
			return;
		}
		
		case 0xFF50:
		{
			use_bios = false;
			break;
		}
	}
	
	mem[addr] = value;
}