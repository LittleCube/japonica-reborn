#include <ppu.h>

#include <memory.h>
#include <drivers.h>

#include <sys/time.h>

const int TRUECOLORS[5][3] = { { 0xFF, 0xA6, 0x30 },
							   { 0xCE, 0x75, 0x00 },
							   { 0xAA, 0x61, 0x00 },
							   { 0x79, 0x45, 0x00 },
							   { 127, 111, 13 } };

int line_ticks;

u8 ly;
u8 intern_wy;

bool vblank;

struct timeval this_time;
struct timeval last_time;

inline void init_ppu()
{
	line_ticks = 0;
	
	ly = 0;
	intern_wy = 0;
	
	vblank = false;
}

inline void ppu_tick(int ticks)
{
	line_ticks += ticks;
	
	if (line_ticks >= 456)
	{
		if (ly < 144)
		{
			scanline();
		}
		
		else if (ly == 144)
		{
			update();
			
			vbl();
			
			intern_wy = 0;
			
			moderate_fps();
		}
		
		else if (ly == 154)
		{
			ly = -1;
			
			// clear bits 0 and 1
			mem[STAT] &= 0xFC;
		}
		
		ly += 1;
		
		if (ly == mem[LYC])
		{
			// set bit 2
			mem[STAT] |= 0x04;
		}
		
		else
		{
			// clear bit 2
			mem[STAT] &= 0xFB;
		}
		
		if ((mem[STAT] & 0x08) != 0 || ((mem[STAT] & 0x40) != 0 && ly == mem[LYC]))
		{
			// set bit 1
			mem[IF] |= 0b10;
		}
		
		line_ticks -= 456;
	}
}

inline void scanline()
{
	u16 bgdata;
	
	if ((mem[LCDC] >> 4) & 1)
	{
		bgdata = 0x8000;
	}
	
	else
	{
		bgdata = 0x9000;
	}
	
	u8 tile_index;
	
	u16 pixels_index;
	
	u8 left;
	u8 right;
	
	bool wy_inc = false;
	
	for (int lx = 0; lx < 160; ++lx)
	{
		u16 bgmap;
		int tile_bit;
		
		if (((mem[LCDC] >> 5) & 1) && lx >= mem[WX] - 7 && ly >= mem[WY])
		{
			// Window
			
			if ((mem[LCDC] >> 6) & 1)
			{
				bgmap = 0x9C00;
			}
			
			else
			{
				bgmap = 0x9800;
			}
			
			int x = lx - (mem[WX] - 7);
			int y = intern_wy;
			
			int tile_x = x >> 3;
			int tile_y = (y >> 3) << 5;
			tile_index = mem[bgmap + tile_x + tile_y];
			
			int true_tile_index;
			
			if (bgdata == 0x9000)
			{
				true_tile_index = (e8) tile_index;
			}
			
			else
			{
				true_tile_index = tile_index;
			}
			
			true_tile_index <<= 4;
			
			int pix_y = y & 7;
			pixels_index = bgdata + true_tile_index + (pix_y << 1);
			
			left = mem[pixels_index];
			right = mem[pixels_index + 1];
			
			tile_bit = 7 - (x & 7);
			
			wy_inc = true;
		}
		
		else
		{
			// BG
			
			if ((mem[LCDC] >> 3) & 1)
			{
				bgmap = 0x9C00;
			}
			
			else
			{
				bgmap = 0x9800;
			}
			
			int x = ((lx + mem[SCX]) & 0xFF);
			int y = ((ly + mem[SCY]) & 0xFF);
			
			int tile_x = (x >> 3);
			int tile_y = ((y >> 3) << 5);
			tile_index = mem[bgmap + tile_x + tile_y];
			
			int true_tile_index;
			
			if (bgdata == 0x9000)
			{
				true_tile_index = (e8) tile_index;
			}
			
			else
			{
				true_tile_index = tile_index;
			}
			
			true_tile_index <<= 4;
			
			int pix_y = ((ly + mem[SCY]) & 7);
			pixels_index = bgdata + true_tile_index + (pix_y << 1);
			
			left = mem[pixels_index];
			right = mem[pixels_index + 1];
			
			tile_bit = 7 - ((lx + mem[SCX]) & 7);
		}
		
		set_bg_color(lx, ly, (((right >> tile_bit) & 1) << 1) | ((left >> tile_bit) & 1));
	}
	
	if (wy_inc)
	{
		++intern_wy;
	}
}

inline void set_spr_color(int x, int y, int color, bool palette)
{
	set_pixel(x, y, TRUECOLORS[spr_color(color, palette)][0], TRUECOLORS[spr_color(color, palette)][1], TRUECOLORS[spr_color(color, palette)][2]);
}

inline int spr_color(int color_select, bool palette)
{
	return (mem[OBP0 | palette] >> (color_select << 1)) & 3;
}

inline void set_bg_color(int x, int y, int color)
{
	set_pixel(x, y, TRUECOLORS[bg_color(color)][0], TRUECOLORS[bg_color(color)][1], TRUECOLORS[bg_color(color)][2]);
}

inline int bg_color(int color_select)
{
	return (mem[LCDC] & 1) != 0 ? ((mem[BGP] >> (color_select << 1)) & 3) : 0;
}

inline void vbl()
{
	mem[IF] |= 1;
	
	// set mem[STAT][1:0] to 0b01
	mem[STAT] &= 0xFC;
	mem[STAT] |= 0x01;
}

inline void moderate_fps()
{
	msleep(7);
}